(function() {
	"use strict";

	var api = {};
	var events;
	var utils;
	var ctConstants;
	var isRegistered;

	api.init = function() {
		events = this.external.eventsDefinition;
		utils = this.external.utils;
		ctConstants = this.external.eventsDefinition.ctConstants;
		addEventListeners();
	};

	function addEventListeners() {
		if (utils.isAnalyticsConfigured()) {
			addListeners();
		}
	}

	function addListeners() {
		if (!isRegistered) {
			Cog.addListener("kritique", events.CLICK.KRITIQUE_HELPFUL, onHelpful);
			Cog.addListener("kritique", events.CLICK.KRITIQUE_READ_REVIEWS, onClick);
			Cog.addListener("kritique", events.CLICK.KRITIQUE_REPORT, onReport);
			Cog.addListener("kritique", events.CLICK.KRITIQUE_WIDGET_CLOSE, onWidgetClose);
			Cog.addListener("kritique", events.CLICK.KRITIQUE_WRITE_REVIEW, onWriteReview);
			Cog.addListener("kritique", events.SUBMIT.KRITIQUE_FORM, onFormSubmit);
			isRegistered = true;
		}
	}

	function onClick(event) {
		trackSingleEvent(
			ctConstants.ratingreview,
			event.eventData.label,
			ctConstants.advocacy,
			ctConstants.interest, {
				position: event.eventData.position
			});
	}

	function onWriteReview(event) {
		trackSingleEvent(
			ctConstants.rrOpen,
			event.eventData.label,
			ctConstants.engagement,
			ctConstants.interest, {
				position: event.eventData.position
			});
	}

	function onWidgetClose(event) {
		trackSingleEvent(
			ctConstants.rrClose,
			event.eventData.label,
			ctConstants.custom,
			ctConstants.interest, {
				position: event.eventData.position
			});
	}

	function onHelpful(event) {
		var eventAction = event.eventData.helpful ? ctConstants.rrHelpful : ctConstants.rrNotHelpful;
		trackSingleEvent(
			eventAction,
			event.eventData.label,
			ctConstants.advocacy,
			ctConstants.interest, {
				position: event.eventData.position
			});
	}

	function onReport(event) {
		trackSingleEvent(
			ctConstants.rrReported,
			event.eventData.label,
			ctConstants.advocacy,
			ctConstants.interest, {
				position: event.eventData.position
			});
	}

	function onFormSubmit(event) {
		trackSingleEvent(
			ctConstants.ratingreview,
			event.eventData.label,
			ctConstants.advocacy,
			ctConstants.interest, {
				position: event.eventData.position
			});
	}

	function trackSingleEvent(action, label, category, subcategory, attributes) { //jshint ignore:line
		var componentPosition = attributes.position || "";
		utils.pushComponent("Kritique", componentPosition, category, subcategory);
		utils.addTrackedEvent(action, label, category, subcategory, attributes);
	}

	Cog.registerStatic({
		name: "analytics.kritiqueHandler",
		api: api,
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
})();
