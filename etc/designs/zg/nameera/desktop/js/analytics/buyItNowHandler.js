(function() {
	"use strict";

	var api = {};
	var events;
	var utils;
	var ctConstants;

	api.init = function() {
		events = this.external.eventsDefinition;
		utils = this.external.utils;
		ctConstants = this.external.eventsDefinition.ctConstants;
		addEventListeners();
	};

	function addEventListeners() {
		if (utils.isAnalyticsConfigured()) {
			Cog.addListener("buyitnow", events.CLICK.BIN_CLICK, binClick);
		}
	}

	function binClick(event) {
		var analyticsProduct = utils.createProduct();
		var platformProduct = allProducts[event.eventData.product] || {};
		var eventLabel = "Online - " + platformProduct.shortTitle;
		var eventData = {
			action: ctConstants.purchase,
			label: eventLabel,
			category: ctConstants.conversion,
			subCategory: ctConstants.lead
		};
		var products = [];
		analyticsProduct.productInfo.productName = platformProduct.shortTitle;
		analyticsProduct.productInfo.productID = platformProduct.EAN;
		analyticsProduct.productInfo.productBrand = digitalData.siteInfo.localbrand;
		analyticsProduct.productInfo.price = platformProduct.productPrice;
		analyticsProduct.productInfo.sku = platformProduct.sku;
		analyticsProduct.category.primaryCategory = platformProduct.category;
		analyticsProduct.attributes.pcatName = platformProduct.category;
		analyticsProduct.attributes.productVariants = platformProduct.sizes;
		products.push(analyticsProduct);
		pushCartEvent(products, eventData, event.eventData.component);
	}

	function pushCartEvent(cartProducts, event, componentName) {
		digitalData.cart = [];
		digitalData.cart.item = [];
		utils.pushCart(cartProducts);
		utils.pushComponent(componentName, "");
		utils.addTrackedEvent(event.action, event.label, event.category, event.subCategory);
	}

	Cog.registerStatic({
		name: "analytics.buyItNowHandler",
		api: api,
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
}());
