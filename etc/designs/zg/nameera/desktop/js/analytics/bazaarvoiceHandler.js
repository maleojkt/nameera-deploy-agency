(function($) {
	"use strict";

	var api = {};
	var events;
	var utils;
	var ctConstants;
	var isRegistered;
	var COMPONENT_POSITION = "";	//component position is not tracked

	api.init = function() {
		events = this.external.eventsDefinition.OTHER;
		utils = this.external.utils;
		ctConstants = this.external.eventsDefinition.ctConstants;
		addEventListeners();
	};

	function addEventListeners() {
		if (utils.isAnalyticsConfigured()) {
			Cog.addListener("bazaarvoice", events.BAZAARVOICE, addBvListeners);
		}
	}

	function addBvListeners() {
		if (!isRegistered) {
			$BV.configure("global", {
				events: {
					submissionSubmitted: function(data) {
						trackSingleEvent(ctConstants.ratingreview, getEventLabel(data),
								ctConstants.advocacy, ctConstants.interest);
					},
					bvRender: function(data) {
						var attributes = {nonInteractive: {nonInteraction: 1}};
						trackSingleEvent(ctConstants.bvrenders, getEventLabel(data),
								ctConstants.custom, ctConstants.read, attributes);
					},
					submissionLoad: function(data) {
						trackSingleEvent(ctConstants.bazaarvoicereviewopen,
								getEventLabel(data), ctConstants.advocacy, ctConstants.interest);
					},
					submissionClose: function(data) {
						trackSingleEvent(ctConstants.bazaarvoicereformcloses,
								getEventLabel(data), ctConstants.advocacy, ctConstants.other);
					}
				}
			});
			isRegistered = true;
		}
	}

	function trackSingleEvent(action, label, category, subcategory, attributes) { //jshint ignore:line
		utils.pushComponent("Bazaarvoice", COMPONENT_POSITION, category, subcategory);
		utils.addTrackedEvent(action, label, category, subcategory, attributes);
	}

	function getEventLabel(data) {
		var product = allProducts[data.Id] || {};
		var title = $('meta[property="og:title"]').attr("content") || "";
		return product.shortTitle || title;
	}

	Cog.registerStatic({
		name: "analytics.bazaarvoiceHandler",
		api: api,
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
}(Cog.jQuery()));
