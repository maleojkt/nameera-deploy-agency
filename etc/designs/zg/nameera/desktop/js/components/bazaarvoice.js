(function($) {
	"use strict";

	var api = {};
	var analyticsDef;
	var analyticsUtils;

	function InlineRating() {
		analyticsSetup();
	}

	function analyticsSetup() {
		Cog.fireEvent("bazaarvoice", analyticsDef.OTHER.BAZAARVOICE);
	}

	$(window).on("load", function() {
		var bazaarvoiceLazyLoad = document.getElementById("bazaarvoice-lazyLoad");
		if (bazaarvoiceLazyLoad) {
			bazaarvoiceLazyLoad.src = bazaarvoiceLazyLoad.getAttribute("data-src");
			bazaarvoiceLazyLoad.removeAttribute("data-src");
			bazaarvoiceLazyLoad.addEventListener("load", function() {
				Cog.fireEvent("bazaarvoice","BV_LAZY_LOAD");
			});
		}
	}.bind(this));

	api.onRegister = function() {
		analyticsDef = this.external.eventsDefinition;
		analyticsUtils = this.external.utils;

		if (typeof $BV !== "undefined") {
			new InlineRating();
		} else {
			Cog.addListener("bazaarvoice", "BV_LAZY_LOAD", analyticsSetup);
		}
	};

	Cog.registerComponent({
		name: "bazaarvoice",
		api: api,
		selector: ".ratingsandreviews",
		requires: [
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			}
		]
	});
})(Cog.jQuery());
