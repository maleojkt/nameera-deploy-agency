(function($) {
	"use strict";

	var api = {};
	var cache = {};
	var filtersHandler;
	var analyticsUtils;
	var ctConstants;
	var breakpoints;

	function ListingCheckBoxesFilter($el) {
		this.$el = $el;
		this.$componentContent = $el.children(".component-content");
		this.$checkboxes = this.$el.find(".listingCheckBoxesFilter-input");
		this.$showMoreButton = $el.find(".js-toggle-filters.more");
		this.$showLessButton = $el.find(".js-toggle-filters.less");
		this.$filterGroupName = $el.find(".listingCheckBoxesFilter-title");
		this.$filterGroupId = CryptoJS.MD5($el.find(".listingCheckBoxesFilter-title").text().trim()).toString();
		this.initiallyShownCheckboxesCount = parseInt(this.$componentContent.data("config").initiallyShownCheckboxesCount, 10);
		this.$expandableCheckboxes = $el.find(".listingCheckBoxesFilter-item:nth-child(" + this.initiallyShownCheckboxesCount + ")").nextAll();
		this.$targetListing = $(".listing");
		this.listingConfig = JSON.parse(this.$targetListing.find("script[type='application/json']").eq(0).text());
		this.basePath = this.listingConfig.path;
		this.componentPosition = analyticsUtils.getComponentPosition(this.$el);
		this.listingPosition = analyticsUtils.getComponentPosition(this.$targetListing);
		this.initializeComponent();
		this.bindUIEvents();
		this.bindEnquire();
	}

	ListingCheckBoxesFilter.prototype = {
		initializeComponent: function() {
			this.$expandableCheckboxes.addClass("is-hidden");
			if (this.$checkboxes.length === 0) {
				this.$componentContent.addClass("is-hidden");
			} else if (this.$checkboxes.length <= this.initiallyShownCheckboxesCount) {
				this.$showLessButton.addClass("is-hidden");
				this.$showMoreButton.addClass("is-hidden");
			}
		},

		bindUIEvents: function() {
			this.$checkboxes.on("change", function(event) {
				this.updateFilters(event, this.$filterGroupId);
			}.bind(this));

			this.$showMoreButton.on("click", function() {
				this.$showMoreButton.addClass("is-hidden");
				this.$showLessButton.removeClass("is-hidden");
				this.$expandableCheckboxes.removeClass("is-hidden");
			}.bind(this));

			this.$showLessButton.on("click", function() {
				this.$showLessButton.addClass("is-hidden");
				this.$showMoreButton.removeClass("is-hidden");
				this.$expandableCheckboxes.addClass("is-hidden");
				this.$el.removeClass("is-active");
			}.bind(this));

			Cog.addListener("checkboxFilters", "updateFilters", function() {
				this.updateFilters(event, this.$filterGroupId);
			}.bind(this));

			Cog.addListener("checkboxFilters", "clearFilter", function() {
				this.clearFilters();
			}.bind(this));
		},

		bindEnquire: function() {
			var clickHandler = this.mobileFilterToggle.bind(this);

			enquire.register("only screen and (max-width: " + breakpoints.maxMobile + "px)", {
				match: function() {
					this.$filterGroupName.on("click", clickHandler);
				}.bind(this),
				unmatch: function() {
					this.$filterGroupName.unbind("click", clickHandler);
					this.initializeComponent();
					this.$el.removeClass("is-active");
				}.bind(this)
			});
		},

		mobileFilterToggle: function() {
			this.$expandableCheckboxes.removeClass("is-hidden");
			this.$showLessButton.removeClass("is-hidden");
			this.$showMoreButton.addClass("is-hidden");
		},

		updateFilters: function(event, groupId) {
			var $clickedCheckbox = $(event.target);
			var tagLabel = $clickedCheckbox.val();
			var isChecked = $clickedCheckbox.prop("checked");

			if (isChecked) {
				filtersHandler.addFilterTag(tagLabel, groupId);
			} else {
				filtersHandler.removeFilterTag(tagLabel, groupId);
			}

			var url = filtersHandler.getGroupedTemplateUrl().replace("{path}", this.basePath);

			filtersHandler.trackCheckboxFilters({
				componentName: "Listing Checkboxes Filter",
				componentPosition: this.componentPosition
			});

			this.getData(url);
		},

		getData: function(path) {
			if (cache[path]) {
				refreshListing(this.$targetListing, cache[path]);
				this.trackProductImpressionChange();
				return;
			}

			this.$el.addClass("loading");

			$.ajax(path, {
				method: "get"
			})
				.always(function() {
					this.$el.removeClass("loading");
				}.bind(this))
				.done(function(data) {
					cache[path] = data;
					refreshListing(this.$targetListing, data);
					Cog.fireEvent("listingCheckBoxesFilter", "filterUpdate");
					this.trackProductImpressionChange();
				}.bind(this));
		},

		trackProductImpressionChange: function() {
			Cog.fireEvent("listing", "Product Impression", {
				componentName: "Listing",
				componentPosition: this.listingPosition,
				products: analyticsUtils.fetchVisibleListingItems(this.$targetListing)
			});
		},

		clearFilters: function() {
			this.$checkboxes.filter(":checked").prop("checked", false);
		}
	};

	function refreshListing($listing, content) {
		$listing
			.html(content)
			.removeClass("initialized");
		Cog.init({$element: $listing});
	}

	api.onRegister = function(scope) {
		filtersHandler = this.external.filters;
		ctConstants = this.external.eventsDefinition.ctConstants;
		analyticsUtils = this.external.utils;
		breakpoints = breakpoints || this.external.breakpoints;
		new ListingCheckBoxesFilter(scope.$scope);
	};

	Cog.registerComponent({
		name: "listingCheckBoxesFilter",
		api: api,
		selector: ".listingCheckBoxesFilter",
		requires: [
			{
				name: "utils.filters",
				apiId: "filters"
			},
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			},
			{
				name: "utils.breakpoints",
				apiId: "breakpoints"
			}
		]
	});
})(Cog.jQuery());
