/**
 * Tiny Slider
 */


(function($) {
	"use strict";

	var api = {};
	var analyticsDef;
    var analyticsUtils;
    

	api.onRegister = function(element) {
		var $element = element.$scope;

		analyticsDef = this.external.eventsDefinition;
        analyticsUtils = this.external.utils;

        // console.log($element);

        // var tes = Array.from($element.get(0));
        // console.log("Jumlha", tes.length);

        var slider = tns({
            container: $element.get(0),
            "items": 3,
            "slideBy": "page",
            "mouseDrag": true,
            "swipeAngle": false,
            "speed": 400,
            cloneCount: 3
          });

         

	};

	Cog.registerComponent({
		name: "TinySlider",
            api: api,
            selector: ".tiny-slider .listing-items",
            requires: [{
                name: "analytics.eventsDefinition",
                apiId: "eventsDefinition"
            },
            {
                name: "analytics.utils",
                apiId: "utils"
            }]
	});
})(Cog.jQuery());
