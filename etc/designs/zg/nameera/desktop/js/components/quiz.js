(function($) {
	"use strict";

	var KEYS = {
		SPACEBAR: 32,
		ENTER: 13
	};

	var PRELOAD = {
		AGRESSIVE: 2, // Load everything, best for small quiz.
		LAZY: 1, // Load one slide at a time, best for large quiz
		DISABLED: 0 // Disable preloading
	};

	var options = {
		indicatorDisabled: false,
		scrollDelay: 500, // used if you want to add any animation before scroll starts
		scrollDuration: 500,
		requestMaxLength: 2048,
		separator: ",",
		tagPrefix: "tags:",
		tagJoinMode: "%20OR%20",
		timer: null,
		selectors: {
			step: ".quiz-step",
			currentStep: ".quiz-step.current",
			firstStep: ".quiz-step[data-step='0']",
			inactiveStep: ".quiz-step[aria-hidden='true']:not([data-step='0'])",
			item: ".quiz-target .quizCTA.component",
			next: ".quizCTA-button",
			previous: ".quiz-buttons-previous",
			reset: ".quiz-buttons-reset",
			indicator: ".quiz-indicator",
			container: ".quiz-container",
			target: ".quiz-target", // where the dynamic content will go
			search: ".searchResults.component",
			hidden: ".is-hidden"
		},
		classNames: {
			active: "is-active",
			current: "current",
			hidden: "is-hidden"
		},
		preloadId: "quiz-preload-content",
		quizStepTemplateId: "quiz_step_template",
		preloadContainerHTML: "<div class=\"$id\" style=\"display:none\"></div>",
		isMobile: false
	};

	var analyticsDef;
	var analyticsUtils;

	function Quiz($el) {
		this.$el = $el;
		this.bindEvents();
		this.init();
		this.position = analyticsUtils.getComponentPosition(this.$el);
	}

	Quiz.prototype = {
		init: function() {
			var observer;
			this.$quizIndiactor = this.$el.find(options.selectors.indicator);
			this.$quizContainer = this.$el.find(options.selectors.container);
			this.startHeight = this.$quizContainer.css("height");
			this.step_template = this.$el.find("." + options.quizStepTemplateId).html();
			this.preLoaded = {};
			this.stage = -1;
			this.options = this.$el.find("[data-options]").data("options");
			this.tags = [];

			options.prevId = this.$el.prev("[id]").attr("id"); // optional, good if it exists
			options = $.extend({}, options, this.options);
			options.preLoadStrategy = parseInt(options.preLoadStrategy || 2, 10);

			this.$quizContainer.css("min-height", this.startHeight);

			this.isMobile();
			this.initIndicator();

			if (options.preLoadStrategy !== PRELOAD.DISABLED) {
				this.$el.append(options.preloadContainerHTML.replace("$id", options.preloadId));
				observer = new MutationObserver(this.contentChanged.bind(this));
				observer.observe(this.$el.find("." + options.preloadId)[0], {
					attributes: false,
					childList: true
				});
				this.preLoad(this.$el.find(options.selectors.currentStep));
			}
		},
		bindEvents: function() {
			var self = this;

			this.$el.on("click keydown", options.selectors.item, function(e) {
				var $currentTarget = $(e.currentTarget);
				var $target = $(e.target);
				if (e.type === "keydown" && !(e.keyCode === KEYS.ENTER || e.keyCode === KEYS.SPACEBAR)) {
					// it's a keypress event and not enter or spacebar so ignore
					return;
				}
				if ($target.closest(options.selectors.next).length === 0) { // ignore clicks on button
					e.preventDefault();
					self.next({
						target: $currentTarget.find(options.selectors.next)[0]
					});
				}
			});
			this.$el.on("click", options.selectors.next, function(e) {
				e.preventDefault();
				self.next(e);
			});
			this.$el.on("click", options.selectors.previous, function(e) {
				e.preventDefault();
				self.previous(e);
			});
			this.$el.on("click", options.selectors.reset, function(e) {
				e.preventDefault();
				self.reset();
			});
		},
		isMobile: function() {
			// don't do transitions on mobile.
			if ("matchMedia" in window && window.matchMedia("(max-width: " + breakpoints.minTablet + "px)").matches) {
				options.isMobile = true;
				options.scrollDelay = 0;
				options.scrollDuration = 0;
			}
		},
		setViewMobile: function() {
			// easy to get lost on mobile so set the
			// view port to the top of the current quiz
			if (options.isMobile && options.prevId) {
				window.location = "#" + options.prevId;
			}
		},
		next: function(e) {
			// user triggered action
			// sets the state
			// and gets the next content
			// updates the step indicator
			var $target = $(e.target);
			var $section = $target.closest(options.selectors.step);
			var $item = $target.closest(options.selectors.item);
			var path = $target.attr("href");
			var tags = $target.data("tags");
			var buttonText = $target.text().trim();

			this.tags.push(tags);

			$item.addClass(options.classNames.active);
			$section.addClass(options.classNames.active);

			this.addPanel({path: path, $section: $section});
			this.indicatorNext();
			this.clickCtaAnalytics(buttonText, path);
		},
		previous: function(e) {
			// user triggered action
			// gets the previous content block back
			// updates the step indicator
			var $target = $(e.target);
			var $section = $target.closest(options.selectors.step);

			this.tags.pop();

			this.shift({
				$section: $section,
				reverse: true
			});
			this.indicatorPrevious();
		},
		reset: function() {
			// user triggered action
			// set state
			// and get the FIRST content block back
			// updates the step indicator
			this.stage = -1;
			this.tags = [];

			this.shift({
				$section: $(options.selectors.currentStep),
				reverse: true,
				$prev: $(options.selectors.firstStep)
			});
			this.$el.find(options.selectors.inactiveStep).remove();
			this.setIndicator();
		},
		checkPreloadContent: function(path) {
			// if the quiz is preloaded any page containing search results
			// won't have the current tags, so we	 need to reload it.
			// we don't know what pages contain in advance
			// so we have to check the content each time
			var HTML = this.preLoaded[path];
			var parseHTML;

			if (!HTML) {
				// it's not loaded
				return false;
			}

			parseHTML = $.parseHTML(HTML);

			if ($(parseHTML).findSelf(options.selectors.search).length > 0) {
				// it's loaded but contains search results panel so
				// delete it and re-request
				parseHTML = null;
				this.preLoaded[path] = "";
				return false;
			}

			return true;
		},
		addPanel: function(ops) {
			// create next panel and load content
			// load content from memory if it's already been requested
			// otherwise request with $.load()
			// then pass to this.shift to swap content
			var $newPanel = $(this.step_template).appendTo(this.$quizContainer);
			var path = ops.path;
			var self = this;

			if (this.checkPreloadContent(path)) {
				// check we have the content for this slide, if not then call $.load()
				$newPanel.find(options.selectors.target).html(this.preLoaded[path]);
				var delay = this.stage === -1 ? 0 : options.scrollDelay ;
				setTimeout(function() {
					self.shift({
						$section: ops.$section
					});
					_.defer(function() {
						self.preLoad($newPanel);
					});
				}, delay);
			} else if (path) {
				$.ajax({
					url: self.getSearchUrl(path),
					method: "GET",
					success: function(data) {
						//creative exchange fix
						if (data.indexOf("<body") !== -1) {
							data = $(data).find(".content")[0].outerHTML;
						}
						self.preLoaded[path] = data;
						$newPanel.find(options.selectors.target).append(data);
						$newPanel.find(options.selectors.hidden).removeClass(options.classNames.hidden);
						self.shift({
							$section: ops.$section
						});
						_.defer(function() {
							self.preLoad($newPanel);
							self.quizResultAnalytics($newPanel);
						});
					}
				});
			}
		},
		quizResultAnalytics: function($markup) {
			var productEans = [];
			$markup.find("[data-ean]").each(function(counter, element) {
				productEans.push($(element).attr("data-ean"));
			});
			Cog.fireEvent("quizResult", analyticsDef.CLICK.QUIZ_RESULT, {
				componentPosition: this.position,
				productEans: productEans,
				slideNumber: this.stage
			});
		},
		clickCtaAnalytics: function(buttonText, nextSlideUrl) {
			var isStartPage = this.stage === 0;

			Cog.fireEvent("quizCta", analyticsDef.CLICK.QUIZ_CTA, {
				componentPosition: this.position,
				nextSlideUrl: nextSlideUrl,
				answer: buttonText,
				slideNumber: this.stage,
				isStartPage: isStartPage
			});
		},
		shift: function(ops) {
			// this method moves the content blocks
			// default direction is to the left
			// move to the right set reverse = true;
			// to implement a different
			// transition e.g. crossfade etc.. change this method
			var $section = ops.$section;
			var $prev = ops.$prev;
			var w = this.$quizContainer.width() * 1.1;
			var $next = $section.next();
			var _out1 = w ;
			var _out2 = w * -1;
			var self = this;

			if (ops.reverse) {
				if ($prev) {
					$next = $prev;
				} else {
					$next = $section.prev();
				}
				_out1 = w * -1;
				_out2 = w;
			}

			if ($next.height() > this.$quizContainer.height()) {
				this.$quizContainer.height($next.height());
			}

			$section.removeClass(options.classNames.current).css("left", 0 + "px").animate({
				left: _out2 + "px"
			}, options.scrollDuration, function() {
				$section.attr("aria-hidden", "true").removeClass(options.classNames.active);
				$section.find("." + options.classNames.active).removeClass(options.classNames.active);
				if (ops.reverse) {
					$section.remove();
				}
			});
			$next.css("left", _out1 + "px").attr("aria-hidden", "false").animate({
				left: 0 + "px"
			}, options.scrollDuration, function() {
				$next.addClass(options.classNames.current);
				Cog.init($next.find(options.selectors.target));
				// the actual CTAs might not be visible so
				// add tabindex to containing block
				$next.find(options.selectors.item)
					.attr("tabindex", "0")
					.attr("role", "button");
				self.$quizContainer.height(""); // remove height declaration
				self.$quizContainer.css("min-height", $next.height() + "px");
				self.setViewMobile();
			});
		},
		initIndicator: function() {
			if (options.indicatorDisabled || this.$quizIndiactor.find("li").length === 0) {
				this.$quizIndiactor.remove();
				return;
			}

			this.setIndicator();
		},
		indicatorNext: function() {
			this.stage = this.stage + 1;
			this.setIndicator();
		},
		indicatorPrevious: function() {
			this.stage = this.stage - 1;
			this.setIndicator();
		},
		setIndicator: function() {
			if (this.indicatorDisabled) {
				return;
			}
			var isVisable = this.stage === -1 ? "true" : "false";

			this.$quizIndiactor
				.attr("aria-hidden", isVisable)
				.find("[aria-current]")
				.removeAttr("aria-current");
			if (this.stage > -1) {
				this.$quizIndiactor
					.find("[data-step=\"" + this.stage + "\"]")
					.attr("aria-current","step");
			}
		},
		contentChanged: function() {
			if (options.preLoadStrategy === PRELOAD.AGRESSIVE) {
				this.preLoad();
			}
		},
		preLoad: function($el) {
			// given an html block find all the links
			// and fetch them (if they aren't in cache
			if (options.preLoadStrategy === PRELOAD.DISABLED) {
				return;
			}
			$el = $el || this.$el.find("." + options.preloadId);
			var $links = $el.find(options.selectors.next + ":not(.prefetch)");
			var when = $.when({});
			var self = this;

			$links.each(function(index, link) {
				var $link = $(link);
				var url = $link.attr("href");
				if (!self.preLoaded[url]) {
					$link.addClass("prefetch");
					when = when.then(self.getDeferred(url));
				}
			});
		},
		getDeferred: function(url) {
			var self = this;
			return function() {
				// wrap with a deferred
				var defer = $.Deferred();

				$.ajax({
					url: self.getUrl(url),
					method: "GET",
					success: function(data) {
						//creative exchange fix
						if (data.indexOf("<body") !== -1) {
							data = $(data).find(".content")[0].outerHTML;
						}
						self.$el.find("." + options.preloadId).append(data);
						self.preLoaded[url] = data;
					},
					error: function(data) {
						data = data || {status: "unknown"};
						self.preLoaded[url] = data.statusText || data.status;
					},
					complete: function() {
						// resolve when complete always.
						// Even on failure we want to keep going with other requests
						defer.resolve();
					}
				});
				// return a promise so that we can chain properly in the each
				return defer.promise();
			};
		},
		getSearchUrl: function(url) {
			var filters = this.encodeTags(this.tags);
			var originLength = location.origin.length; // get length of domain name
			var prefix = url.includes("?") ? "&" : "?";
			var qs = prefix + "q=";
			var cqPath = this.getUrl(url);
			var hrefLength = originLength + cqPath.length + qs.length;
			var filtersMaxLength = options.requestMaxLength - hrefLength;

			// check request is not too long, if so
			// drop tags from the begging until it's ok
			while (filters.length > filtersMaxLength) {
				this.tags.shift();
				filters = this.encodeTags(this.tags);
			}

			cqPath = cqPath + qs + filters;
			return cqPath;
		},
		getUrl: function(url) {
			url = url.replace(".html","/_jcr_content/content.html");
			return url;
		},
		encodeTags: function(tags) {
			var encodedTags = [];
			$.each(tags, function(index, tag) {
				if (tag) {
					var escapedColons = tag.split(":").join("\\:");
					escapedColons = this.parseMultipleTags(escapedColons);
					encodedTags.push(options.tagPrefix + "(*" + escapedColons + "*)");
				}
			}.bind(this));
			return encodedTags.join(options.tagJoinMode);
		},
		parseMultipleTags: function(multipleTag) {
			var multipleTagsJoin = "*)" + options.tagJoinMode + options.tagPrefix + "(*";
			return multipleTag.split(",").join(multipleTagsJoin);
		}
	};

	var breakpoints;
	var api = {
		onRegister: function(scope) {
			breakpoints = this.external.breakpoints;
			analyticsDef = this.external.eventsDefinition;
			analyticsUtils = this.external.utils;
			new Quiz(scope.$scope);
		}
	};

	Cog.registerComponent({
		name: "product.selector.quiz",
		api: api,
		selector: ".quiz.component",
		requires: [{
			name: "utils.breakpoints",
			apiId: "breakpoints"
		},
		{
			name: "analytics.eventsDefinition",
			apiId: "eventsDefinition"
		},
		{
			name: "analytics.utils",
			apiId: "utils"
		}]
	});

})(Cog.jQuery());
