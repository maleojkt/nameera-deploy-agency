/**
 * Flickity Carousel
 */


 (function($) {
	"use strict";

	var api = {};
	var analyticsDef;
	var analyticsUtils;

	api.onRegister = function(element) {
		var $element = element.$scope;

		analyticsDef = this.external.eventsDefinition;
        analyticsUtils = this.external.utils;
        
        var slider = new Flickity( $element.get(0), { 
            cellAlign: 'left',
            freeScroll: true,
            // setGallerySize: false,
            // imagesLoaded: true
            // risize: false
        })

	};

	Cog.registerComponent({
		name: "FlickityCarousel",
            api: api,
            selector: ".free-scroll .listing-items",
            requires: [{
                name: "analytics.eventsDefinition",
                apiId: "eventsDefinition"
            },
            {
                name: "analytics.utils",
                apiId: "utils"
            }]
	});
})(Cog.jQuery());
