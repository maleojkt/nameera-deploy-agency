(function() {
	"use strict";

	var api = {};

	function Shopalyst($el) {
		this.showButton = $el.data("show-button");
		this.type = $el.data("type");
		this.placeholderId = $el.data("placeholder-id");
		this.eanNumber = $el.data("ean");
		this.$button = $el.find(".shopalyst-btn");
		this.shopalystProperties = {
			type: this.type
		};
		if (typeof this.showButton !== "undefined") {
			this.init();
		}
	}

	Shopalyst.prototype = {
		init: function() {
			if (this.showButton) {
				this.validate();
				this.bindUIEvents();
			} else {
				this.show();
			}
		},
		show: function() {
			if (typeof _shopalyst !== "undefined") {
				switch (this.type) {
					case "merchantSelector":
						_shopalyst.addEANToCart(this.eanNumber);
						break;
					case "merchantWidget":	//jshint ignore:line
					default:
						_shopalyst.showBinWidget(this.eanNumber, this.placeholderId, this.shopalystProperties);
						break;
				}
			}
		},
		validate: function() {
			if (typeof this.eanNumber === "undefined" || typeof this.placeholderId === "undefined" || typeof this.type === "undefined") {
				this.$button.attr("disabled", true);
			}
		},
		bindUIEvents: function() {
			this.$button.bind("click", function(event) {
				event.preventDefault();
				this.show();
			}.bind(this));
		}
	};

	api.onRegister = function(scope) {
		new Shopalyst(scope.$scope);
	};

	Cog.registerComponent({
		name: "shopalyst",
		api: api,
		selector: ".shopalyst"
	});
}());
