/**
 * Form
 */

(function($) {
	"use strict";

	var api = {};
	var clearForm;
	var resetForm;
	var manualInputs;
	var analyticsDef;
	var analyticsUtils;
	var formUtils;

	clearForm = function($form, oldbrowser) {
		$form.find("input:text, input:password, input:file, select, textarea").val("");
		$form.find("input:radio").prop("checked", false);
		$form.find("input:checkbox").prop("checked", false);

		if (oldbrowser) {
			$form.find("input:text, input:password, input:file, select, textarea").blur();
		}
	};

	resetForm = function($form) {
		$form[0].reset();
		$form.find("input, textarea").blur();
	};

	manualInputs = (function() {
		function applyStyle($input, $label) {
			if ($input.is(":checked")) {
				$label.addClass("checked");
			} else {
				$label.removeClass("checked");
			}
		}

		return function($inputs) {
			$inputs.each(function(i, input) {
				var $input = $(input),
					$label = $input.parent().find("label");

				$label.on("click", function(ev) {
					ev.preventDefault();
					input.checked = !input.checked;
				});

				applyStyle($input, $label);
				$input.on("change", function() {
					applyStyle($input, $label);
				});
			});
		};
	})();

	api.onRegister = function(scope) {
		analyticsDef = this.external.eventsDefinition;
		analyticsUtils = this.external.utils;
		formUtils = this.external.formUtils;

		var $form = scope.$scope,
			disabledMessage = "Submit functionality is disabled" +
				" in Edit mode - switch to Preview mode to use it.",
			browser = this.external.browser,
			status = this.external.status;

		$form.find("textarea[maxlength]").bind("change keyup", function() {
			var maxlength = $(this).attr("maxlength"),
				val = $(this).val();

			if (val.length > maxlength) {
				$(this).val(val.substring(0, maxlength));
			}
		});

		manualInputs($form.find("input[type=radio], input[type=checkbox]"));

		$form.find("button.clearButton").bind("click", function() {
			clearForm($(this.form));
		});
		if (browser.msie && browser.version < 10) {
			$form.on("click", "button.clearButton", function() {
				clearForm($(this.form), true);
			});
			$form.find("input.reset").bind("click", function(e) {
				e.preventDefault();
				resetForm($(this.form));
			});
			$form.find("button.editSubmit").bind("click", function(e) {
				if (status.isAuthor()) {
					return true;
				}
				e.preventDefault();
				window.alert(disabledMessage);
			});
		} else {
			$form.on("click", "button.clearButton", function() {
				clearForm($(this.form));
			});
			$form.find("button.editSubmit").bind("click", function(e) {
				if (status.isAuthor()) {
					return true;
				}
				e.preventDefault();
				window.alert(disabledMessage);
			});
		}

		setupForm($form);
	};

	function setupForm($form) {
		var formType = formUtils.determineFormType($form);
		switch (formType) {
			case formUtils.FORM_TYPE.CONTACT_US:
				setupDcsForm($form, {
					submitLabel: "Contact Us Form Submitted",
					submitAction: analyticsDef.ctConstants.forms,
					pageLoadLabel: "Contact Us Form Start",
					pageLoadAction: analyticsDef.ctConstants.contactUsStart
				});
				break;
			case formUtils.FORM_TYPE.SIGN_UP:
				setupDcsForm($form, {
					submitLabel: "Sign Up Form Submitted",
					submitAction: analyticsDef.ctConstants.forms,
					pageLoadLabel: "Sign Up Start",
					pageLoadAction: analyticsDef.ctConstants.signupStart
				});
				break;
			default:
				setupDefault($form);
		}
	}

	function setupDcsForm($form, config) {
		//default config
		config = config || {
			submitLabel: "Form Submitted",
			submitAction: analyticsDef.ctConstants.forms,
			pageLoadLabel: "Form Start",
			pageLoadAction: analyticsDef.ctConstants.forms
		};
		var name = formUtils.getFormName($form);
		var position = analyticsUtils.getComponentPosition($form.find("form")) || 0;
		var submitLabel = config.submitLabel + " | " + name;
		var pageLoadLabel = config.pageLoadLabel + " | " + name;

		onDcsSubmit($form, submitLabel, config.submitAction, position);
		$(document).ready(function() {
			fireFormEvent(analyticsDef.LOAD.FORM_PAGE, pageLoadLabel,
				config.pageLoadAction,
				position);
		});
	}

	function setupDefault($form) {
		var idValue = $form.find("a[id]").attr("id") || "";
		var name = formUtils.getFormName($form);
		var position = analyticsUtils.getComponentPosition($form.find("form")) || 0;
		var eventLabel = name + " - " + idValue;
		var eventAction = analyticsDef.ctConstants.forms;

		$form.on("submit", function() {
			fireFormEvent(analyticsDef.CLICK.FORM_CLICK, eventLabel, eventAction, position);
		});
	}

	function onDcsSubmit($form, eventLabel, eventAction, position) {
		$form.on("submit", function(e) {
			sendToDcs($form, e);
			fireFormEvent(analyticsDef.CLICK.FORM_CLICK, eventLabel, eventAction, position);
		});
	}

	function onDcsSuccess($form) {
		var formType = formUtils.determineFormType($form);
		var formName = formUtils.getFormName($form);
		var position = analyticsUtils.getComponentPosition($form.find("form")) || 0;
		if (formType === formUtils.FORM_TYPE.CONTACT_US) {
			fireFormEvent(analyticsDef.SUBMIT.FORM,
				"Contact us Form Submit | " + formName,
				analyticsDef.ctConstants.contactUsSubmit,
				position);
		} else {
			fireFormEvent(analyticsDef.SUBMIT.FORM,
				"Successful Sign Up | " + formName,
				analyticsDef.ctConstants.signupSubmit,
				position);
		}
	}

	function getCheckboxQuestionAnswers($form) {
		var questionAnswers = {};
		$form.find("input[type='checkbox'][name^='question'][name*='answer']").each(function(key, element) {
			var property = $(element).attr("name");
			var value = element.checked;
			questionAnswers[property] = value;
		});
		return questionAnswers;
	}

	function sendToDcs($form, event) {
		event.preventDefault();
		var $formElement = $form.find("form");
		var $formWrapper = $form.find(">.component-content");

		var dcsGatewayAddress = "/sk-eu/services/dcs";
		var successRedirect = $formWrapper.data("success-redirect");
		var errorRedirect = $formWrapper.data("error-redirect");
		var adapterParams = JSON.stringify($formWrapper.data("adapter-params"));
		var userFormData = formUtils.arrayToObject($formElement.serializeArray());
		var formData = $.extend(userFormData, getCheckboxQuestionAnswers($formElement));
		formData.adapterParams = adapterParams;

		if (formUtils.isFormValid($formElement) && formUtils.isCaptchaValidOrNotExist($form)) {
			$.ajax({
				type: $formElement.attr("method"),
				url: dcsGatewayAddress,
				data: formData,
				contentType: "application/x-www-form-urlencoded",
				success: function(response) {
					if (response.success) {
						onDcsSuccess($form);
						Cog.Cookie.create("UnileverID", response.unileverId, response.unileverIdTtlInDays);
						window.location.href = successRedirect + ".html";
					} else {
						window.location.href = errorRedirect + ".html";
					}
				},
				error: function() {
					window.location.href = errorRedirect + ".html";
				}
			});
		}
	}

	function fireFormEvent(eventType, eventLabel, eventAction, position) {
		Cog.fireEvent("form", eventType, {
			label: eventLabel,
			action: eventAction || analyticsDef.ctConstants.forms,
			componentPosition: position
		});
	}

	Cog.registerComponent({
		name: "form",
		api: api,
		selector: ".form",
		requires: [
			{
				name: "utils.browser",
				apiId: "browser"
			},
			{
				name: "utils.status",
				apiId: "status"
			},
			{
				name: "analytics.eventsDefinition",
				apiId: "eventsDefinition"
			},
			{
				name: "analytics.utils",
				apiId: "utils"
			},
			{
				name: "utils.form",
				apiId: "formUtils"
			}
		]
	});
}(Cog.jQuery()));
