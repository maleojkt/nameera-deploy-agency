/**
 * Form type recognising
 */

(function() {
	"use strict";
	var api = {},
		sharedApi = {};

	var CLASSES = {
		DYNAMIC_FORM: "dynamicForm",
		SIGN_UP_FORM: "sign-up-form",
		EMAIL_US_FORM: "email-us-form"
	};

	sharedApi.FORM_TYPE = {
		SIGN_UP: "SIGN_UP",
		CONTACT_US: "CONTACT_US",
		OTHER: "OTHER"
	};

	sharedApi.determineFormType = function($form) {
		var type = sharedApi.FORM_TYPE.OTHER;
		if (sharedApi.isDynamicForm($form) && $form.hasClass(CLASSES.SIGN_UP_FORM)) {
			type = sharedApi.FORM_TYPE.SIGN_UP;
		} else if (sharedApi.isDynamicForm($form) && $form.hasClass(CLASSES.EMAIL_US_FORM)) {
			type = sharedApi.FORM_TYPE.CONTACT_US;
		}
		return type;
	};

	sharedApi.isDynamicForm = function($form) {
		return $form.hasClass(CLASSES.DYNAMIC_FORM);
	};

	sharedApi.isFormValid = function($form) {
		return $form.find(":invalid").length === 0;
	};

	sharedApi.isCaptchaValidOrNotExist = function($form) {
		if ($form.find(".g-recaptcha-response").length && typeof grecaptcha !== "undefined") {
			return grecaptcha.getResponse().length !== 0;
		}
		return true;
	};

	sharedApi.getFormName = function($form) {
		return $form.find("form").attr("name") || "Form without name";
	};

	sharedApi.arrayToObject = function(formArray) {
		var resultObject = {};
		for (var i = 0; i < formArray.length; i++) {
			resultObject[formArray[i].name] = formArray[i].value;
		}
		return resultObject;
	};

	Cog.registerStatic({
		name: "utils.form",
		api: api,
		sharedApi: sharedApi
	});
})();
