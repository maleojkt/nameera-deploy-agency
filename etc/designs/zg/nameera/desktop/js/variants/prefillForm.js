/**
 * Components - Prefilling sign up form with an email from query string
 */
(function($) {
	"use strict";

	var api = {};
	var formClass = ".form--redirect";
	var emailInput = ":input#email";
	var emailQueryParam = "email";

	api.init = function() {
		$(formClass)
			.find(emailInput)
			.val(this.external.querystring.getFromQueryString(emailQueryParam));
	};

	Cog.register({
		name: "prefill-form",
		api: api,
		selector: formClass,
		requires: [
			{
				name: "utils.querystring",
				apiId: "querystring"
			}
		]
	});

})(Cog.jQuery());

