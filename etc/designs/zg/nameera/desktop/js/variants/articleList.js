(function($) {
	"use strict";

	var api = {};
	var objectFit;

	function ArticleList($articleList) {
		this.$articleList = $articleList;
		this.$itemWrapper = this.$articleList.find(".listing-item");
		this.$listingItem = this.$itemWrapper.find("> .component-content");

		this.$listingItem.on("click", function() {
			var $this = $(this),
				$link = $this.find("a").last();

			$link.trigger("click");
		});

		this.$listingItem.find("a").on("click", function(e) {
			e.stopPropagation();
			window.location.href = $(this).attr("href");
		});

		objectFit.polyfill(this.$itemWrapper, ".image-small > .component-content");
	}

	api.onRegister = function(scope) {
		objectFit = this.external.objectFit;
		new ArticleList(scope.$scope);
	};

	Cog.registerComponent({
		name: "article-list",
		api: api,
		selector: ".listing-article-list",
		requires: [{
				name: "utils.objectFit",
				apiId: "objectFit"
			}
		]
	});
})(Cog.jQuery());
