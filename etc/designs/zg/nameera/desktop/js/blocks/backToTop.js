/**
 * Back To Top
 */
(function($) {

	"use strict";

	var api = {};

	api.onRegister = function(scope) {
		var $button = scope.$scope;

		$button.on("click", function(e) {
			e.preventDefault();

			$("html, body").animate({
				scrollTop: 0
			}, 700);
		});
	};

	Cog.registerComponent({
		name: "backToTop",
		api: api,
		selector: ".reference-icon-link .back-to-top"
	});

}(Cog.jQuery()));
