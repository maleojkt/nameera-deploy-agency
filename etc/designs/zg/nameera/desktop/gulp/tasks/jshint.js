/* jshint node:true */

'use strict';

const gulp = require('gulp');
const jshint = require('gulp-jshint');

gulp.task('jshint:dev', function() {
	return gulp.src(['js/**/*.js', '!js/polyfills/*.js', '!js/head/*.js', 'js/analytics/*.js', '!js/libs/*.js'])
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
});

gulp.task('jshint:build', function() {
	return gulp.src(['js/**/*.js', '!js/head/*.js', '!js/libs/*.js', 'js/analytics/*.js', '!js/polyfills/*.js'])
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(jshint.reporter('fail'));
});
