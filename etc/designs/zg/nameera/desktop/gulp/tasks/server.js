/* jshint node:true */

'use strict';

const gulp = require('gulp');
const localWebServer = require('local-web-server');
const path = require('path');
const browserSync = require('browser-sync').create();

gulp.task('server', ['dev'], function () {
	var contentDir = "../../../../../",
		mockDir = path.resolve('mock'),
		formattedMockDir = mockDir.substring(mockDir.indexOf('etc')-1).replace(/\\/g,"/"),
		wsOptions = {
			rewrite: [
				{
					from: "*.lightbox.html",
					to: "$1.html"
				},
				{
					from: "*/product-quick-view.*.html",
					to: "$1/creative-exchange/product-quick-view.html"
				},
				{
					from: "*/where-to-buy/_jcr_content/*.html",
          to: formattedMockDir + "/listing.html"
				},
				{
					from: "/quiz-ce*/_jcr_content/content.html*",
					to: "$1.html"
				}
			],
			static: {
				root: contentDir
			},
			serveIndex: {
				path: contentDir
			}
		};

	localWebServer(wsOptions).listen(8000);
});

gulp.task('style-watch', ['styles'], function(done){
	browserSync.reload();
	done();
})

gulp.task('js-watch', ['jshint:dev', 'jscs:dev', 'concat'], function(done){
	browserSync.reload();
	done();
})

gulp.task('serve', ['dev'], function(){
	var contentDir = '../../../../../';
	browserSync.init({
		server: {
			baseDir: [contentDir],
			index: "content/brands/nameera/id/id/home.html"
		}
	})

	gulp.watch('css/**/*.css', ['style-watch']);
	gulp.watch(['js/**/*.js', '!js/head/*.js'], ['js-watch']);
	gulp.watch(['js/head/*.js','!js/head/head.js'], ['js-watch']);
})
